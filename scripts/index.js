import { button, form, locationInput, results } from './declarations.js';

import { search, setValueOnLocalStore } from './api.js';
import { debounce } from './debounce.js';
import { throttle } from './throttle.js';
//form control

form.addEventListener('submit', (e) => {
  e.preventDefault();
});

//On input call debounce function

locationInput.addEventListener(
  'input',
  debounce((e) => output(e.target.value))
);

//On click call throttle function

button.addEventListener(
  'click',
  throttle(() => setValueOnLocalStore(locationInput.value))
);

//Search autocomplete

const output = async (value) => {
  if (value !== '') {
    const matches = await search(value);

    if (matches.length > 0) {
      const html = matches
        .map(
          (match) => `
                <div class='result'>
                <span>${match.title}</span>
                </div>
                `
        )
        .join('');
      results.style.display = 'block';
      results.innerHTML = html;

      let divResults = document.querySelectorAll('.result');
      divResults.forEach((result) => {
        result.addEventListener('click', () => {
          setValueOnLocalStore(result.textContent.trim());
        });
      });
    } else results.innerHTML = 'No location found';
  } else results.innerHTML = '';
};

//On Ckick button
