//Throttle function
const throttle = (fn) => {
  let last;
  return function (...args) {
    const now = new Date().getTime();
    if (now - last < 2000) {
      return;
    }
    last = now;
    return fn(...args);
  };
};

export { throttle };
