// Index.html

import { city, forecast, locationInput, results } from './declarations.js';
import { convertDate } from './date.js';

/// Information with given woeid (where on earth id)

const getLocationInfo = (woeid) => {
  const data = fetch(
    `https://api.allorigins.win/raw?url=https://www.metaweather.com/api/location/${woeid}/`
  )
    .then((res) => {
      if (!res.ok) {
        throw new Error(`HTTP error: ${res.status}`);
      }
      return res.json();
    })
    .then((res) => {
      return res;
    })
    .catch((error) => alert(error));
  return data;
};

// Search given word

const search = (word) => {
  const data = fetch(
    `https://api.allorigins.win/raw?url=https://www.metaweather.com/api/location/search/?query=${word}`
  )
    .then((res) => res.json())
    .then((data) => {
      const titles = data.map((location) => {
        return {
          title: location.title,
          woeid: location.woeid,
        };
      });
      return titles;
    })
    .catch((error) => console.log(error));

  return data;
};

//Set woeid on localstorage
const setValueOnLocalStore = async (value) => {
  if (!value) {
    const result = `write a location`;
    results.innerHTML = result;
    return;
  }
  const data = await search(value);
  if (data.length === 0) {
    const result = `<h4>No location match ${value} </h4>`;
    results.style.display = 'flex';
    results.innerHTML = result;
    return;
  }
  localStorage.setItem('woeid', data[0].woeid);
  results.style.display = 'none';
  locationInput.value = '';
  getInfo();
};

//get location information
const getInfo = async () => {
  const woeid = parseInt(localStorage.getItem('woeid'));
  const { consolidated_weather: data, title } = await getLocationInfo(woeid);
  let all = '';
  const cityName = `<h3>${title}</h3>`;

  for (let i = 0; i < data.length; i++) {
    all += ` <div class='card'>
      <span><strong>${convertDate(data[i].applicable_date)}</strong></span>
      <img src="./images/${data[i].weather_state_abbr}.svg">
      <span>${data[i].weather_state_name}</span>
      <span><strong>${Math.round(
        data[i].the_temp
      )}<small>°C</small></strong></span>
      </div>`;
  }
  city.innerHTML = cityName;
  forecast.innerHTML = all;
};
getInfo();

export { getLocationInfo, search, setValueOnLocalStore, getInfo };
