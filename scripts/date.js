const month = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

const convertDate = (date) => {
  const today = new Date();
  const tomorrow = today.getDate() + 1;
  const weatherDay = new Date(date);
  let format = `${days[weatherDay.getDay()]} ${weatherDay.getDate()} ${
    month[weatherDay.getMonth()]
  } `;

  if (weatherDay.getDate() === today.getDate()) format = 'Today';
  if (weatherDay.getDate() === tomorrow) format = 'Tomorrow';

  return format;
};

export { convertDate };
