const locationInput = document.getElementById('location');
const results = document.getElementById('results');
const forecast = document.getElementById('forecast');
const button = document.getElementById('button');
const form = document.getElementById('form');
const city = document.getElementById('city');
const idStored = localStorage.getItem('woeid');

// if woeid is empty then asign san francisco woeid
if (!idStored) {
  localStorage.setItem('woeid', '2487956');
}

export { locationInput, results, forecast, button, form, city };
